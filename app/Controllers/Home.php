<?php

namespace App\Controllers;

class Home extends BaseController
{

    public function __construct(){
        
        $this->isLogin();

    }

    public function index()
    {
        
       
        $data['title'] = 'Dashboard';
        $data['breadcrumbs'] = array('dashboard');
        $data['vue'] = 'dashboard';
        $data['content'] = view('content/dashboard/index');
        return view('layout/index' , $data);

    }
}