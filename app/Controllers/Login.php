<?php

namespace App\Controllers;

class Login extends BaseController
{

    
    public function index()
    {   

        $this->isLogin();
        if(!$this->validate([])){
            $data= [
                'validation' => $this->validator,
            ];
            return view('content/auth/login' , $data);
        }
    }

    public function proses(){

        $validated = $this->validate(
            [
                'username' => 'required',
                'password' => 'required',
            ],
            [
                'username' => [
                    'required' => 'Username tidak boleh kosong',
                ],
                'password' => [
                    'required' => 'Password tidak boleh kosong',
                ],
            ]
        );

        if ($validated == FALSE) {
            
            return $this->index();

        }else{

            $session = session();

            $username = $this->request->getPost('username');
            $password =$this->request->getPost('password');

            $usernamedb = \NataHelpers::getUser($username);
            $passworddb = \NataHelpers::getUser($username , 'password');
            if($usernamedb == $username){

                if(password_verify($password , $passworddb)){

                    $token = \NataHelpers::createtoken();
                    
                    if(\NataHelpers::updatetokenindb($username , $token)){
                        $ses_data = [
                            'token'     => $token,
                        ];
                        $session->set($ses_data);
                        return redirect()->to('/dashboard');
                    }else{
                        $session->setFlashdata('error', 'Gagal update token. Silahkan coba login kembali');
                        return redirect()->to('/login');
                    }

                }else{
                    $session->setFlashdata('error', 'Kombinasi password tidak sesuai');
                    return redirect()->to('/login');
                }

            }else{
                $session->setFlashdata('error', 'Username tidak terdaftar');
                return redirect()->to('/login');
            }

        }

    }

    public function logout(){

        $token =session()->get('token');
        $session = session();
        $session->destroy();
        \NataHelpers::removetokenindb($token);
        return redirect()->to('/login');

    }
}
