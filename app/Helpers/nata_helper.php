<?php

    class NataHelpers{

        static public function getUser($username='' , $getfield="username"){
            if(!empty($username)){
                $db   = \Config\Database::connect();
                $result = $db->query("SELECT * FROM users where username = '$username'")->getRowArray();
                if($result){
                    return $result["$getfield"];
                }else{
                    return '';
                }
            }
        }

        static public function getProfile($token='' , $getfield="username"){
            $db   = \Config\Database::connect();
            $result = $db->query("SELECT * FROM users where token = '$token'")->getRowArray();
            if($result){
                return $result["$getfield"];
            }else{
                return '';
            }
        }
        
        static public function createtoken(){
            $n = 20;
            $result = bin2hex(random_bytes($n));   
            return $result; 
        }
        
        static public function updatetokenindb($username , $token){
            $db   = \Config\Database::connect();
            $query = $db->query("UPDATE users set token = '$token' where username = '$username'");
            if($query){
                return true;
            }else{
                return false;
            }
        }
        
        static public function removetokenindb($token){
            $db   = \Config\Database::connect();
            $query = $db->query("UPDATE users set token = null where token = '$token'");
            if($query){
                return true;
            }else{
                return false;
            }
        }
        
        static public function templateerror($message = '' , $type="input"){
        
            if($type== 'input'){
                if(!empty($message)){
                    $html ="<small style='color:red; font-style: italic; font-size:12px; font-weight: bold;'>*$message</small>";
                    return $html;
                }
            }else{
                
                if(!empty(session()->getFlashdata())){

               
                    if(session()->getFlashdata('error')){
                        $flashmessage = session()->getFlashdata('error');
                        $html ="<div class='sufee-alert alert with-close alert-danger alert-dismissible fade show'>
                        <span class='badge badge-pill badge-danger'>Gagal</span>
                        $flashmessage
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                        </div>";
                        return $html;
                    }else{
                        $flashmessage = session()->getFlashdata('error');
                        $html ="<div class='sufee-alert alert with-close alert-success alert-dismissible fade show'>
                        <span class='badge badge-pill badge-success'>Berhasil</span>
                        $flashmessage
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                        </div>";
                        return $html;
                    }
                }
        
            }
        


        }

        static public function menus(){
            $db   = \Config\Database::connect();
            $query = $db->query("SELECT * FROM menus")->getResultArray();
            return $query;
        }

}