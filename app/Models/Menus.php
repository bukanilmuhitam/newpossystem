<?php namespace App\Models;

use CodeIgniter\Model;

class Menus extends Model
{

    protected $table    = 'menus';
    protected $primaryKey = 'id_menu';

    protected $allowedFields = ['nama_menu' , 'is_main_menu' , 'url' , 'id_main_menu' , 'icon' , 'set_menu_active' , 'level'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

}