<?php namespace App\Models;

use CodeIgniter\Model;

class Users extends Model
{

    protected $table    = 'users';
    protected $primaryKey = 'id_user';

    protected $allowedFields = ['username' , 'email' , 'password' , 'level' , 'token'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

}