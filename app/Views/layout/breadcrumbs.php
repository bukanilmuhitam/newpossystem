<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><?=empty($title) ? 'P.O.S' : $title ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <?php 
                                foreach($breadcrumbs as $breadcrumb){
                            ?>
                                <?php 
                                    if(end($breadcrumbs) == $breadcrumb){
                                ?>
                                    <li class="active"><?=$breadcrumb?></li>
                                <?php }else{?>
                                    <li><a href="<?=$breadcrumb?>"><?=$breadcrumb?></a></li>
                                <?php }?>
                            <?php }?>
                           
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>