
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="menu-title">Main Menu</li>
                <?php 
                    $menus = NataHelpers::menus();
                    $submenus = NataHelpers::menus();
                    $page = str_replace('/' , '' , $_SERVER['REQUEST_URI']);
                    $level = NataHelpers::getProfile(session()->get('token') , 'level');
                    foreach($menus as $menu){
                        if($menu['is_main_menu'] == '1'){
                            $set_menu_active = explode(',' , $menu['set_menu_active']);
                        }else{
                            $set_menu_active = $menu['set_menu_active'];
                        }
                        
                        if(empty($menu['role'])){

                            $akses = true;
                        
                        }else{
                            if(strpos($menu['role'] , ',')){
                                $role = explode(',' , $menu['role']);

                                if(in_array($level , $role)){
                                    
                                    $akses = true;

                                }else{

                                    $akses = false;

                                }

                            }else{
                                $role = $menu['role'];

                                if($role == $level){

                                    $akses = true;

                                }else{

                                    $akses = false;

                                }
                            }
                        }
                
                ?>
                    
                    <?php 
                        if($akses){
                    ?>

                    <?php 
                        if(empty($menu['id_main_menu'])){
                    ?>
                        <?php 
                            if($menu['is_main_menu'] == '1'){

                                if(in_array($page , $set_menu_active)){
                                    $show = 'show';
                                    $active = 'active';
                                }else{
                                    $show = '';
                                    $active = '';
                                }

                        ?>

                            <li class="menu-item-has-children dropdown <?=$active?> <?=$show?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false"> <i class="menu-icon <?=$menu['icon']?>"></i><?=$menu['nama_menu']?></a>
                                <ul class="sub-menu children dropdown-menu <?=$show?>">
                                    <?php 
                                        foreach($submenus as $submenu){
                                    ?>

                                        <?php 
                                            if(!empty($submenu['id_main_menu'])){

                                                if(empty($submenu['role'])){

                                                    $submenuakses = true;
                                                
                                                }else{
                                                    if(strpos($submenu['role'] , ',')){
                                                        $role = explode(',' , $submenu['role']);
                        
                                                        if(in_array($level , $role)){
                                                            
                                                            $submenuakses = true;
                        
                                                        }else{
                        
                                                            $submenuakses = false;
                        
                                                        }
                        
                                                    }else{
                                                        $role = $submenu['role'];
                        
                                                        if($role == $level){
                        
                                                            $submenuakses = true;
                        
                                                        }else{
                        
                                                            $submenuakses = false;
                        
                                                        }
                                                    }
                                                }
                                        ?>
                                            <?php 
                                                if($submenuakses){
                                            ?>
                                                <?php 
                                                    if($submenu['id_main_menu'] == $menu['id_menu']){
                                                ?>

                                                    <li><i class="fa fa-circle-o"></i><a href="/<?=$submenu['url']?>" class="<?php if($page == $submenu['url']){ echo "text-primary"; }else{ echo ""; } ?>"><?=$submenu['nama_menu']?></a></li>

                                                <?php }?>
                                            <?php }?>
                                        <?php }?>
                                    <?php }?>
                                </ul>
                            </li>

                        <?php }else{?>

                            <li class="<?php if($page == $set_menu_active){ echo "active"; }else{ echo ""; }?>">
                                <a href="/<?=$menu['url']?>"><i class="menu-icon <?=$menu['icon']?>"></i><?=$menu['nama_menu']?> </a>
                            </li>

                        <?php }?>
                    <?php }?>
                <?php }?>
                <?php }?>
            

            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>