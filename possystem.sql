/*
 Navicat Premium Data Transfer

 Source Server         : root
 Source Server Type    : MySQL
 Source Server Version : 100421
 Source Host           : localhost:3306
 Source Schema         : possystem

 Target Server Type    : MySQL
 Target Server Version : 100421
 File Encoding         : 65001

 Date: 25/10/2021 23:36:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `is_main_menu` int(11) NULL DEFAULT NULL,
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `id_main_menu` int(11) NULL DEFAULT NULL,
  `icon` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `set_menu_active` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `role` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id_menu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (5, 'Dashboard', NULL, 'dashboard', NULL, 'fa fa-laptop', 'dashboard', NULL);
INSERT INTO `menus` VALUES (6, 'Master Data', 1, NULL, NULL, 'fa fa-table', 'kategori,satuanbarang', '');
INSERT INTO `menus` VALUES (7, 'Kategori Barang', NULL, 'kategoribarang', 6, NULL, NULL, '');
INSERT INTO `menus` VALUES (8, 'Satuan Barang', NULL, 'satuanbarang', 6, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `level` int(2) NULL DEFAULT NULL,
  `token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', NULL, '$2y$10$GGpl1/icBTZEvHYyhbrx8.RlkEIpSfXazH6ms/IDFkmP0DiL31VKC', 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
